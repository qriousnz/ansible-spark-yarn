ansible-spark-yarn
=========

Installs Apache Spark and Yarn onto a cluster

Requirements
------------
- Ansible 2.5.2 or newer

Role Variables
--------------
`cluster_role`: master or worker

`masters`: a list of masters

`workers`: a list of workers

`hdfs_bind_address`: The IP address to bind to
